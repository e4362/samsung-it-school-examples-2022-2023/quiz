package ru.dolbak.creativity;

import java.util.Arrays;

public class Question {
    String questionText;
    int amountOfAnswers = 4;
    int correctAnswer;
    String[] answers;
    int selectedAnswer = -1;
    boolean isCorrect = false;

    public Question(String questionText, int correctAnswer,
                    String answer0, String answer1, String answer2,
                    String answer3){
        this.questionText = questionText;
        this.correctAnswer = correctAnswer;
        answers = new String[amountOfAnswers];
        answers[0] = answer0;
        answers[1] = answer1;
        answers[2] = answer2;
        answers[3] = answer3;
    }

    @Override
    public String toString() {
        return "Question{" +
                "questionText='" + questionText + '\'' +
                ", amountOfAnswers=" + amountOfAnswers +
                ", correctAnswer=" + correctAnswer +
                ", answers=" + Arrays.toString(answers) +
                ", selectedAnswer=" + selectedAnswer +
                '}';
    }
}
