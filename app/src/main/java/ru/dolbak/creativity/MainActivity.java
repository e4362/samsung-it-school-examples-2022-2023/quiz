package ru.dolbak.creativity;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    Question[] questions;
    int questionNow = 0;
    int radioIds[] = {R.id.radio1, R.id.radio2, R.id.radio3, R.id.radio4};
    TextView questionText, answerText;
    RadioGroup radioGroup;
    int questionsAmount = 5;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        questionText = findViewById(R.id.question);
        answerText = findViewById(R.id.answer);
        radioGroup = findViewById(R.id.radioGroup);
        ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setIsIndicator(true);
        questions = new Question[questionsAmount];
        questions[0] = new Question("На каком языке" +
                " Google рекомендую писать Android приложения",
                1, "Java", "Kotlin", "Python", "Ruby");
        questions[1] = new Question("Что не является языком программирования?",
                3, "Go", "Ruby", "PHP", "HTML");
        questions[2] = new Question("Сколько депутатов избирается в Госдуму РФ?",
                2, "45", "100", "450", "1000");
        questions[3] = new Question("Какой ветви власти нет в РФ?",
                1, "Законодательной", "Бюрократический",
                "Судебной", "Исполнительной");
        questions[4] = new Question("В каком году выпустили Windows XP?",
                2, "1999", "2000", "2001", "2007");
        putQuestion(questionNow);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        return;
                    case R.id.radio1:
                        questions[questionNow].selectedAnswer = 0;
                        break;
                    case R.id.radio2:
                        questions[questionNow].selectedAnswer = 1;
                        break;
                    case R.id.radio3:
                        questions[questionNow].selectedAnswer = 2;
                        break;
                    case R.id.radio4:
                        questions[questionNow].selectedAnswer = 3;
                        break;
                    default:
                        break;
                }
            }
        });
    }

    void putQuestion(int n){
        radioGroup.clearCheck();
        questionText.setText(questions[n].questionText);
        for (int i = 0; i < questions[n].amountOfAnswers; i++){
            RadioButton radioButton = findViewById(radioIds[i]);
            radioButton.setText(questions[n].answers[i]);
        }
        if (questions[n].selectedAnswer >= 0){
            RadioButton myRadioButton = findViewById(radioIds[questions[n].selectedAnswer]);
            myRadioButton.setChecked(true);
        }

    }

    public void onClickMove(View view) {
        if (view.getId() == R.id.prev_btn){
            if (questionNow > 0){
                putQuestion(questionNow - 1);
                questionNow -= 1;
            }
        }
        if (view.getId() == R.id.next_btn){
            if (questionNow < questionsAmount - 1){
                putQuestion(questionNow + 1);
                questionNow += 1;
            }
            else if (questionNow == questionsAmount - 1){
                int sum = 0;
                for (Question question: questions){
                    if (question.isCorrect){
                        sum++;
                    }
                }
                ratingBar.setRating((float) (sum * 1.0 / questionsAmount) * 5);
            }
        }
        Log.d("NIKITA", Arrays.toString(questions));
    }


    public void onClickCheck(View view) {
        int checkButton = radioGroup.getCheckedRadioButtonId();
        int ansNum = -1;
        switch (checkButton){
            case R.id.radio1:
                ansNum = 0;
                break;
            case R.id.radio2:
                ansNum = 1;
                break;
            case R.id.radio3:
                ansNum = 2;
                break;
            case R.id.radio4:
                ansNum = 3;
                break;
            default:
                break;
        }
        if (ansNum == questions[questionNow].correctAnswer){
            questions[questionNow].isCorrect = true;
            answerText.setText("Ваш ответ правильный");
            answerText.setTextColor(Color.GREEN);
        }
        else{
            questions[questionNow].isCorrect = false;
            answerText.setText("Ваш ответ НЕправильный");
            answerText.setTextColor(Color.RED);
        }
    }
}